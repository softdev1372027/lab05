/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package com.warathip.lab5;

import com.warathip.lab5.Board;
import com.warathip.lab5.Player;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author win10
 */
public class OXGameUI extends javax.swing.JFrame {

    /**
     * Creates new form OXGameUI
     */
    public OXGameUI() {
        initComponents();
        this.o = new Player('O');
        this.x = new Player('X');
        load();
        showWelcome();
        newGame();
        showStat();
    }

    public void load() {
        FileInputStream fis = null;
        try {
            File file = new File("player.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            this.o = (Player) ois.readObject();
            this.x = (Player) ois.readObject();
            ois.close();
            fis.close();
        } catch (Exception ex) {
            System.out.println("Load Error!!!");
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(OXGameUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void save() {
        try {
            FileOutputStream fos = null;
            
            File file = new File("player.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(o);
            oos.writeObject(x);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(OXGameUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(OXGameUI.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btntable4 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        txtmessage = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        btntable02 = new javax.swing.JButton();
        btntable03 = new javax.swing.JButton();
        btntable07 = new javax.swing.JButton();
        btntable01 = new javax.swing.JButton();
        btntable06 = new javax.swing.JButton();
        btntable04 = new javax.swing.JButton();
        btntable08 = new javax.swing.JButton();
        btntable09 = new javax.swing.JButton();
        btntable05 = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        txtPlayerX = new javax.swing.JLabel();
        txtPlayerO = new javax.swing.JLabel();
        btnnewgame = new javax.swing.JButton();
        txtOwin = new javax.swing.JLabel();
        txtOdraw = new javax.swing.JLabel();
        txtOloss = new javax.swing.JLabel();
        txtXwin = new javax.swing.JLabel();
        txtXdraw = new javax.swing.JLabel();
        txtXloss = new javax.swing.JLabel();

        btntable4.setFont(new java.awt.Font("Segoe UI", 0, 36)); // NOI18N
        btntable4.setText("-");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel4.setBackground(new java.awt.Color(255, 255, 153));

        txtmessage.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        txtmessage.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtmessage.setText("Welcome To OX Game");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtmessage, javax.swing.GroupLayout.DEFAULT_SIZE, 464, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(22, Short.MAX_VALUE)
                .addComponent(txtmessage)
                .addGap(18, 18, 18))
        );

        jPanel5.setBackground(new java.awt.Color(255, 204, 204));

        btntable02.setFont(new java.awt.Font("Segoe UI", 0, 36)); // NOI18N
        btntable02.setText("-");
        btntable02.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btntable02ActionPerformed(evt);
            }
        });

        btntable03.setFont(new java.awt.Font("Segoe UI", 0, 36)); // NOI18N
        btntable03.setText("-");
        btntable03.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btntable03ActionPerformed(evt);
            }
        });

        btntable07.setFont(new java.awt.Font("Segoe UI", 0, 36)); // NOI18N
        btntable07.setText("-");
        btntable07.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btntable07ActionPerformed(evt);
            }
        });

        btntable01.setFont(new java.awt.Font("Segoe UI", 0, 36)); // NOI18N
        btntable01.setText("-");
        btntable01.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btntable01ActionPerformed(evt);
            }
        });

        btntable06.setFont(new java.awt.Font("Segoe UI", 0, 36)); // NOI18N
        btntable06.setText("-");
        btntable06.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btntable06ActionPerformed(evt);
            }
        });

        btntable04.setFont(new java.awt.Font("Segoe UI", 0, 36)); // NOI18N
        btntable04.setText("-");
        btntable04.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btntable04ActionPerformed(evt);
            }
        });

        btntable08.setFont(new java.awt.Font("Segoe UI", 0, 36)); // NOI18N
        btntable08.setText("-");
        btntable08.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btntable08ActionPerformed(evt);
            }
        });

        btntable09.setFont(new java.awt.Font("Segoe UI", 0, 36)); // NOI18N
        btntable09.setText("-");
        btntable09.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btntable09ActionPerformed(evt);
            }
        });

        btntable05.setFont(new java.awt.Font("Segoe UI", 0, 36)); // NOI18N
        btntable05.setText("-");
        btntable05.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btntable05ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btntable01, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btntable04, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(btntable02, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btntable03, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(btntable05, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btntable06, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(8, 8, 8))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(btntable07, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btntable08, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btntable09, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(8, Short.MAX_VALUE))))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btntable02, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btntable03, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btntable01, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btntable04, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btntable05, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btntable06, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btntable07, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btntable08, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btntable09, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel6.setBackground(new java.awt.Color(153, 204, 255));

        txtPlayerX.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txtPlayerX.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtPlayerX.setText("Player X");

        txtPlayerO.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txtPlayerO.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtPlayerO.setText("Player O");

        btnnewgame.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        btnnewgame.setText("New Game");
        btnnewgame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnnewgameActionPerformed(evt);
            }
        });

        txtOwin.setBackground(new java.awt.Color(153, 255, 153));
        txtOwin.setText("Win : 0");
        txtOwin.setOpaque(true);

        txtOdraw.setBackground(new java.awt.Color(153, 153, 255));
        txtOdraw.setText("Draw : 0");

        txtOloss.setBackground(new java.awt.Color(255, 102, 102));
        txtOloss.setText("Loss : 0");
        txtOloss.setOpaque(true);

        txtXwin.setBackground(new java.awt.Color(153, 255, 153));
        txtXwin.setText("Win : 0");
        txtXwin.setOpaque(true);

        txtXdraw.setBackground(new java.awt.Color(153, 153, 255));
        txtXdraw.setText("Draw : 0");

        txtXloss.setBackground(new java.awt.Color(255, 102, 102));
        txtXloss.setText("Loss : 0");
        txtXloss.setOpaque(true);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(txtXwin, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtXdraw, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtXloss, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnnewgame, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtPlayerO, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtPlayerX, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                                .addGap(0, 6, Short.MAX_VALUE)
                                .addComponent(txtOwin, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtOdraw, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtOloss, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(16, 16, 16)))
                        .addContainerGap())))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(txtPlayerO)
                .addGap(20, 20, 20)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtOwin)
                    .addComponent(txtOdraw)
                    .addComponent(txtOloss))
                .addGap(18, 18, 18)
                .addComponent(txtPlayerX)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtXwin)
                    .addComponent(txtXdraw)
                    .addComponent(txtXloss))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnnewgame, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(63, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btntable03ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btntable03ActionPerformed
        row = 1;
        col = 3;
        process();
    }//GEN-LAST:event_btntable03ActionPerformed

    private void btntable01ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btntable01ActionPerformed
        row = 1;
        col = 1;
        process();
    }//GEN-LAST:event_btntable01ActionPerformed


    private void btntable02ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btntable02ActionPerformed
        row = 1;
        col = 2;
        process();
    }//GEN-LAST:event_btntable02ActionPerformed

    private void btntable04ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btntable04ActionPerformed
        row = 2;
        col = 1;
        process();
    }//GEN-LAST:event_btntable04ActionPerformed

    private void btntable05ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btntable05ActionPerformed
        row = 2;
        col = 2;
        process();
    }//GEN-LAST:event_btntable05ActionPerformed

    private void btntable06ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btntable06ActionPerformed
        row = 2;
        col = 3;
        process();
    }//GEN-LAST:event_btntable06ActionPerformed

    private void btntable07ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btntable07ActionPerformed
        row = 3;
        col = 1;
        process();
    }//GEN-LAST:event_btntable07ActionPerformed

    private void btntable08ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btntable08ActionPerformed
        row = 3;
        col = 2;
        process();
    }//GEN-LAST:event_btntable08ActionPerformed

    private void btntable09ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btntable09ActionPerformed
        row = 3;
        col = 3;
        process();
    }//GEN-LAST:event_btntable09ActionPerformed

    private void btnnewgameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnnewgameActionPerformed
        newGame();
    }//GEN-LAST:event_btnnewgameActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(OXGameUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(OXGameUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(OXGameUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(OXGameUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new OXGameUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnnewgame;
    private javax.swing.JButton btntable01;
    private javax.swing.JButton btntable02;
    private javax.swing.JButton btntable03;
    private javax.swing.JButton btntable04;
    private javax.swing.JButton btntable05;
    private javax.swing.JButton btntable06;
    private javax.swing.JButton btntable07;
    private javax.swing.JButton btntable08;
    private javax.swing.JButton btntable09;
    private javax.swing.JButton btntable4;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JLabel txtOdraw;
    private javax.swing.JLabel txtOloss;
    private javax.swing.JLabel txtOwin;
    private javax.swing.JLabel txtPlayerO;
    private javax.swing.JLabel txtPlayerX;
    private javax.swing.JLabel txtXdraw;
    private javax.swing.JLabel txtXloss;
    private javax.swing.JLabel txtXwin;
    private javax.swing.JLabel txtmessage;
    // End of variables declaration//GEN-END:variables
private Player o;
    private Player x;
    private int row;
    private int col;
    private Board board;

    private void showWelcome() {
        txtmessage.setText("Welcome To OX Game");
    }

    private void newBoard() {
        board = new Board(o, x);
    }

    private void showTable() {
        char[][] table = board.getTable();
        btntable01.setText(table[0][0] + "");
        btntable02.setText(table[0][1] + "");
        btntable03.setText(table[0][2] + "");
        btntable04.setText(table[1][0] + "");
        btntable05.setText(table[1][1] + "");
        btntable06.setText(table[1][2] + "");
        btntable07.setText(table[2][0] + "");
        btntable08.setText(table[2][1] + "");
        btntable09.setText(table[2][2] + "");
    }

    private void showTurn() {
        txtmessage.setText("Turn" + board.getCurrentPlayer().getSymbol());
    }

    private void process() {
        board.setRowCol(row, col);
        showTable();
        showTurn();
        if (isFinish()) {
            showTable();
            showResult();
            showStat();
            save();
        }
    }

    private boolean isFinish() {
        if (board.isDraw() || board.isWin()) {
            return true;
        }
        return false;
    }

    private void showResult() {
        if (board.isDraw()) {
            txtmessage.setText("Draw!!!");
        } else if (board.isWin()) {
            txtmessage.setText(board.getCurrentPlayer().getSymbol() + " Win");
        }
    }

    private void newGame() {
        newBoard();
        showTable();
        showTurn();
    }

    private void showStat() {
        txtOwin.setText("Win:" + o.getWin());
        txtOdraw.setText("Draw:" + o.getDraw());
        txtOloss.setText("Loss:" + o.getLoss());
        txtXwin.setText("Win:" + x.getWin());
        txtXdraw.setText("Draw:" + x.getDraw());
        txtXloss.setText("Loss:" + x.getLoss());
    }
}
